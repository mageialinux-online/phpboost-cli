# MLO

Some tools on MLO

## phpboost-cli

Untested CLI tools managing the phpboost CMS.

### Example
```bash
#!/bin/bash
source ./phpboost-cli.bash

read -s -p "Phpboost an admin user: " phpboost_user
read -s -p "Phpboost an admin pwd: " phpboost_user_pwd

maintenance_msg="We are under maintainance, sorry for disruption.
The admins."

# Get our server ip:
var=( $(host www.phpboost.com) )
server_ip=${var[-1]}

phpboost_connect ${server_ip} "www.phpboost.com" "${phpboost_user}" "${phpboost_user_pwd}"
phpboost_set_maintainance "${maintenance_msg}"
phpboost_disconnect

exit 0
```

### Why is there a server ip and a domain name ?

Because we used this tool to migrate a server, the domain name was pointing only to one server and during the propagation, we need to communicate with both servers using vhost.
