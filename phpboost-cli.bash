phpboost_msg_encode() {
    echo "${@//$'\n'/$'\r\n'}" | xxd -p -c 0 | sed -e 's|\([[:xdigit:]]\{2\}\)|%\1|g'
}

phpboost_cookie_jar=''
phpboost_token=''
phpboost_user_agent="User-Agent: ${BASH_SOURCE[-1]##*/}"
phpboost_website=''
phpboost_vhost=''
server_ip=''



function phpboost_connect () {
    server_ip="${1}"
    phpboost_website="${2}"
    phpboost_login="${3}"
    phpboost_user_pwd="${4}"
    phpboost_cookie_jar=${phpboost_cookie_jar:-$(mktemp)}
    phpboost_get "https://${phpboost_website}/login/" -X POST --data-raw "login=${phpboost_login}&password=${phpboost_user_pwd}&authenticate=internal"
    phpboost_get "https://${phpboost_website}/login/" -X POST --data-raw "login=${phpboost_login}&password=${phpboost_user_pwd}&authenticate=internal"
    phpboost_get "https://${phpboost_website}/login/" -X POST --data-raw "login=${phpboost_login}&password=${phpboost_user_pwd}&authenticate=internal"
}

function phpboost_get() {
    local url=${*}
    if [[ ${*} =~ .*authenticate=.* ]] ; then
        url=$(echo "${url}" | sed -e "s|authenticate=internal|authenticate=internal\&token=${phpboost_token}|")
    fi
    if [[ ${*} =~ .*disconnect=true.* ]] ; then
        url=$(echo "${url}" | sed -e "s|disconnect=true|disconnect=true\&token=${phpboost_token}\&valid=true |")
    fi
    if [[ ${*} =~ .*new=n_msg.* ]] ; then
        url=$(echo "${url}" | sed -e "s|new=n_msg|new=n_msg\&token=${phpboost_token}|")
    fi
    if [[ ${*} =~ .*AdminMaintainController_disabled_fieldsets=.* ]] ; then
        url=$(echo "${url}" | sed -e "s|AdminMaintainController_disabled_fieldsets=|AdminMaintainController_disabled_fieldsets=\&token=${phpboost_token}|")
    fi
    if [[ ${*} =~ .*del=1.* ]] ; then
        url=$(echo "${url}&token=${phpboost_token}")
    fi
    page=$(mktemp)
    curl -i -k -L \
        --resolve "${phpboost_website}:80:${server_ip}" \
        --resolve "${phpboost_website}:443:${server_ip}" \
        -c "${phpboost_cookie_jar}" -b "${phpboost_cookie_jar}" \
        --compressed \
        -H "${phpboost_user_agent}" \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
        -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' \
        -H 'Content-Type: application/x-www-form-urlencoded' \
        ${url} -o ${page}
    potential_phpboost_token="$( sed -e '/^var TOKEN/!d;s|^.*"\([[:xdigit:]]\+\)".*$|\1|;q' ${page})"
    phpboost_token="${potential_phpboost_token:-${phpboost_token}}"
    cat ${page}
    rm -f ${page}
}

function phpboost_read_topic() {
    phpboost_thread="${1}"
    phpboost_get "https://${phpboost_website}/forum/topic-${phpboost_thread}.php"
}

function phpboost_add_msg() {
    phpboost_thread="${1}"
    shift 1
    local content_msg="$(phpboost_msg_encode "${*}")"
    phpboost_get "https://${phpboost_website}/forum/post.php?idt=${phpboost_thread}&id=70&new=n_msg" -X POST --data-raw "content=${content_msg}&valid=true"
}

function phpboost_del_msg() {
    local phpboost_msg="${1}"
    phpboost_get "https://${phpboost_website}/forum/action.php?del=1&idm=${phpboost_msg}"
}

function phpboost_set_maintainance() {
    local content_msg="$(phpboost_msg_encode "${*}")"
    phpboost_get "https://${phpboost_website}/admin/maintain/" -X POST --data-raw "AdminMaintainController_maintain_type=unlimited&AdminMaintainController_message=${content_msg}&groups_authAdminMaintainController_authorizations1%5B%5D=r1&groups_authAdminMaintainController_authorizations1%5B%5D=r2&loginAdminMaintainController_authorizations1=&AdminMaintainController_submit=true&AdminMaintainController_disabled_fields=%7Cmaintain_during%7Cmaintain_until%7Cdisplay_duration_for_admin&AdminMaintainController_disabled_fieldsets="
}

function phpboost_reset_maintainance() {
    phpboost_get "https://${phpboost_website}/admin/maintain/" -X POST --data-raw  'AdminMaintainController_maintain_type=disabled&AdminMaintainController_message=done&groups_authAdminMaintainController_authorizations1%5B%5D=r1&groups_authAdminMaintainController_authorizations1%5B%5D=r2&loginAdminMaintainController_authorizations1=&AdminMaintainController_submit=true&AdminMaintainController_disabled_fields=%7Cmaintain_during%7Cmaintain_until%7Cdisplay_duration_for_admin&AdminMaintainController_disabled_fieldsets='
}

function phpboost_disconnect () {
    phpboost_get "https://${phpboost_website}/login/?disconnect=true"
    rm -f ${phpboost_cookie_jar}
    phpboost_cookie_jar=''
    phpboost_token=''
    phpboost_website=''
    phpboost_vhost=''
    server_ip=''
}

